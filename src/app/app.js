/**
 * The Main module
 */
angular.module("styleGuide", [
    'ui.bootstrap'
])

    /**
     * Main controller
     */
    .controller('styleguideCtrl', ['$scope', function AlertDemoCtrl($scope) {

        // Drop down items
        $scope.ddItems = [
            "The first choice!",
            "And another choice for you.",
            "but wait! A third!"
        ];

    }])

;