# [Angular-base](https://bitbucket.org/smich/angular-base)

A simple [angular](http://angularjs.org) setup for quick prototyping.

***

## Quick Start

*Notice: Installation/usage notes for Ubuntu 13.10*

Install node and npm:

```sh
sudo apt-get install nodejs 
sudo ln -s /usr/bin/nodejs /usr/bin/node 
curl https://npmjs.org/install.sh | sudo sh
```
Then fork the [angular-base](https://bitbucket.org/smich/angular-base), and follow the instructions below to setup the environment in your local:

```sh
$ # Clone the forked repo in your local
$ git clone git@bitbucket.org:your-username/angular-base.git angular-base
$ cd angular-base
$ # Fetch the latest code from upstream
$ git fetch origin
$ # Checkout the latest master
$ git checkout origin/master -b master
$ # Install the required npm modules globally
$ sudo npm -g install grunt-cli
$ # Install the npm modules required by the application; they 'll be saved under **/node_modules**
$ npm install
$ # Compile the application and see what happens when you change watched resources (i.e js and css and html files). Enjoy...
$ grunt watch
```

Now you just need a server to serve the app. For convenience reason a node web server added in the
app, launch it:

```sh
node scripts/web-server.js
```

Finally, open `localhost:8000/build/index.html` in your browser.

Happy hacking!

## Extras

### Grunt

It takes care of the whole build process, you can read more [here](https://github.com/joshdmiller/ng-boilerplate#detailed-installation).

### Live Reload

The app includes [Live Reload](http://livereload.com/), so you no longer have to refresh your page after making changes! You simply need a Live Reload browser plugin for this:

- Chrome - [Chrome Webstore](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei)
- Firefox - [Download from Live Reload](http://download.livereload.com/2.0.8/LiveReload-2.0.8.xpi)
- Safari - [Download from Live Reload](http://download.livereload.com/2.0.9/LiveReload-2.0.9.safariextz)

You can find more info about Live Reload [here](https://github.com/joshdmiller/ng-boilerplate#live-reload).
